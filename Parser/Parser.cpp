#include "Parser.h"


const vector<Token> Parser::tokens = {
    Token("( ){4}", Tag::SEPARATOR),
    Token("[ ]", Tag::UNDEFINED),
    Token("\\n", Tag::EOL),
    Token(",", Tag::COMMA),
    Token("[+|-]?(\\d+)?\\.\\d+", Tag::REAL),
    Token(
        "(if)|(then)|(else)|(while)|(def)|(extern)|(return)|(then)",
         Tag::KEYWORD
    ),
    Token("([A-Z]|[a-z]|[0-9]|_)+", Tag::IDENTIFIER),
    Token("(\\-)|(\\+)|(/)|(\\*)|(==)|(=)", Tag::OPERATOR),
    Token("(\\()|(\\))", Tag::BRACKETS),
};
