#include "AST.h"


FunCallExprAST::FunCallExprAST(const string& func, vector<BaseAST*>&& args)
    : BaseAST(), func_name(func), args_list(args) {}
FunCallExprAST::~FunCallExprAST() {}


string FunCallExprAST::str() const {
    string args = "(";
    const size_t len = args_list.size();
    if (len != 0) {
        const size_t last = len - 1;
        for (int i = 0; i < last; i++)
            args += args_list[i]->str() + ", ";
        args += args_list[last]->str();
    }
    return string(
        "[Function Call Expression: name - " + func_name +
        ", arguments - " + args + ")]"
    );
}
