#include "AST.h"


FunctionAST::FunctionAST(PrototypeAST* proto, BaseAST* _body) :
    BaseAST(), prototype(proto), body(_body) {}
FunctionAST::~FunctionAST() {}


string FunctionAST::str() const {
    return string(
        "[Function " + prototype->str() + "\n\tbody - " + body->str() + ")]"
    );
}
