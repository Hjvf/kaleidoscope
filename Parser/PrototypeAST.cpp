#include "AST.h"


PrototypeAST::PrototypeAST(const string& name, vector<BaseAST*>&& args) :
    BaseAST(), func_name(name), args_list(args) {}
PrototypeAST::~PrototypeAST() {}


string PrototypeAST::str() const {
    string args = "(";
    const size_t len = args_list.size();
    if (len != 0) {
        const size_t last = len - 1;
        for (int i = 0; i < last; i++)
            args += args_list[i]->str();
        args += args_list[last]->str();
    }
    return string(
        "[Funtion Prototype: name - " + func_name +
        ", arguments - " + args + ")]"
    );
}
