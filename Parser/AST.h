#ifndef AST_H
#define AST_H

#include <string>
using std::string;
using std::to_string;
#include <vector>
using std::vector;
#include <ostream>
using std::ostream;
#include <utility>
using std::move;


class BaseAST {
public:
    BaseAST() = default;
    virtual string str() const = 0;
};

/* number */
class NumberAST : public BaseAST {
    double value;

public:
    NumberAST(const double val);
    ~NumberAST();

    double getValue() const { return value; }
    string str() const override;
};


/* variable */
class VariableAST : public BaseAST {
    string identifier;

public:
    VariableAST(const string& id);
    ~VariableAST();

    string getIdentifier() const { return identifier; }
    string str() const override;
};

/* x <= 10 */
class BinaryExprAST : public BaseAST {
    string op;
    BaseAST* left_expr;
    BaseAST* right_expr;

public:
    BinaryExprAST(const string& _op, BaseAST* left, BaseAST* right);
    ~BinaryExprAST();

    string getOperator() const { return op; }
    /* Owner BinaryExprAST */
    BaseAST* getLeft() const { return left_expr; }
    BaseAST* getRight() const { return right_expr; }

    string str() const override;
};


/* sin(x) call */
class FunCallExprAST : public BaseAST {
    string func_name;
    vector<BaseAST*> args_list;

public:
    FunCallExprAST(const string& func, vector<BaseAST*>&& args);
    ~FunCallExprAST();

    string getFunctionName() const { return func_name; }
    /* owner FuncCallExprAST */
    vector<BaseAST*> getArgumentsList() const;

    string str() const override;
};


/* def f(x, y) */
class PrototypeAST : public BaseAST {
    string func_name;
    vector<BaseAST*> args_list;

public:
    PrototypeAST(const string& name, vector<BaseAST*>&& args);
    ~PrototypeAST();

    string getFunctionName() const { return func_name; }
    /* Owner PrototypeAST */
    vector<BaseAST*> getArgumentsList() const;

    string str() const override;
};

/* def add(a, b) return a + b */
class FunctionAST : public BaseAST {
    PrototypeAST* prototype;
    BaseAST* body;

public:
    FunctionAST(PrototypeAST* proto, BaseAST* _body);
    ~FunctionAST();

    /* owner FunctionAST */
    PrototypeAST* getPrototype() const { return prototype; }
    BaseAST* getBody() const { return body; }

    string str() const override;
};
#endif // AST_H
