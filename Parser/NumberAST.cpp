#include "AST.h"


NumberAST::NumberAST(const double val) : BaseAST(), value(val) {}
NumberAST::~NumberAST() {}


string NumberAST::str() const {
    return string("[Number: " + to_string(value) + "]");
}
