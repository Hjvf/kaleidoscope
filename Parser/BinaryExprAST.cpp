#include "AST.h"


BinaryExprAST::BinaryExprAST(const string& _op, BaseAST* left, BaseAST* right) :
    BaseAST(), op(_op), left_expr(left), right_expr(right) {}

BinaryExprAST::~BinaryExprAST() {}


string BinaryExprAST::str() const {
    return string(
        "[Binary Expression: operator - " + op +
        "\n\tLeft subexpr - " + left_expr->str() +
        "\n\tRight subexpr - " + right_expr->str() + "]"
    );
}
