#ifndef PARSER_H
#define PARSER_H

#include "../Lexer/Lexer.h"
#include "AST.h"

class Parser {
    static const vector<Token> tokens;

    BaseAST* ast = nullptr;
    vector<Lexem> lexems;

public:
    Parser() = default;
    ~Parser() = default;

    void parse(const string& str);

private:
    BaseAST* parseNumber(const unsigned int pos) const;
    BaseAST* parseVariable(const unsigned int pos) const;
    BaseAST* parseBinariExpr(const unsigned int pos) const;
    BaseAST* parseFunCallExpr(const unsigned int pos) const;
    BaseAST* parseFunctionAST(const unsigned int pos) const;
    BaseAST* parsePrototypeAST(const unsigned int pos) const;
};


#endif // PARSER_H
