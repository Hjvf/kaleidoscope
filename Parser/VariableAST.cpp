#include "AST.h"


VariableAST::VariableAST(const std::string& id) : BaseAST(), identifier(id) {}
VariableAST::~VariableAST() {}


string VariableAST::str() const {
    return string("[Variable: " + identifier + "]");
}
