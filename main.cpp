#include <iostream>

#include "Lexer/Lexer.h"
#include "Parser/AST.h"
using std::cout;
using std::endl;

int main() {
    Lexem lexem(string("string"), 0, 9, Tag::IDENTIFIER);
    cout << lexem << endl;
    Lexem _lexem(Lexem(string("other"), 0, 9, Tag::KEYWORD));
    cout << _lexem << endl;

    NumberAST number(34.5);
    cout << number.str() << endl;

    return 0;
}
