#include "Lexer.h"


Token::Token(const char* _template, const Tag& _tag) :
    pattern(_template), tag(_tag), string_pattern(_template) {}
Token::Token(const string& _template, const Tag& _tag) :
    pattern(_template), tag(_tag), string_pattern(_template) {}
Token::~Token() {}


Lexem Token::match(const string& str, const unsigned int position) const {
    using CIter = string::const_iterator;
    using CAllocator = allocator<sub_match<CIter>>;
    match_results<CIter, CAllocator> match_object;

    bool result = std::regex_search(
        str.begin() + position, str.end(), match_object, pattern
    );
    return result && match_object.position() == 0 ?
        Lexem(match_object.str(), position, match_object.length(), tag) :
        Lexem();
}


ostream& operator << (ostream& os, const Token& token) {
    return os << "[Token: Pattern - " << token.getPattern()
              << " Tag - " << token.getTag();
}


bool operator == (const Token& left, const Token& right) {
    return (
        left.getTag() == right.getTag() &&
        left.getPattern() == right.getPattern()
    );
}


bool operator != (const Token& left, const Token& right) {
    return !(left == right);
}
