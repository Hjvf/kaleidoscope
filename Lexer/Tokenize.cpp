#include "Lexer.h"


vector<Lexem> tokenize(const string &str, const vector<Token> &tokens) {
    size_t position = 0;
    size_t length = str.size();
    vector<Lexem> lexems;

    if (length == 0)
        return lexems;

    bool any_match;
    while (position < length) {
        any_match = true;
        for (const Token& token: tokens) {
            Lexem match_result = token.match(str, position);
            if (!match_result.isEmpty()) {
                any_match = false;
                position += match_result.getLength();
                lexems.push_back(match_result);
                break;
            }
        }
        if (any_match)
            throw SyntaxError(str, position);
    }

    return lexems;
}
