#ifndef LEXER_H
#define LEXER_H

#include <regex>
using std::regex;
using std::match_results;
using std::sub_match;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <memory>
using std::allocator;
#include <ostream>
using std::ostream;
#include <sstream>
using std::stringstream;
#include <exception>


class SyntaxError : public std::exception {
    string str;
    size_t position;

public:
    SyntaxError(const string& _str, const size_t _position) : std::exception() {
        str = _str; position = _position;
    }

    virtual const char* what() {
        string reason(
            string("Invalid token at ") + std::to_string(position) +
            string(" in string ") + str
        );
        return reason.c_str();
    }
};


enum class Tag {
    UNDEFINED = -1,
    SEPARATOR = 0,
    REAL = 1,
    KEYWORD = 2,
    IDENTIFIER = 3,
    OPERATOR = 4,
    BRACKETS = 5,
    COMMA = 6,
    EOL = 7
};

ostream& operator << (ostream& os, const Tag& tag);



class Lexem {
    string content;
    unsigned int position;
    unsigned int length;
    Tag tag;

public:
    Lexem();
    Lexem(
        const string& _content, const unsigned int _pos,
        const unsigned int _length, const Tag& _tag
    );

    void set(
        const string& _content, const unsigned int _pos,
        const unsigned int _length, const Tag& _tag
    );

    string getContent() const { return content; }
    unsigned int getPosition() const { return position; }
    unsigned int getLength() const { return length; }
    Tag getTag() const { return tag; }

    bool isEmpty() const;
};

bool operator == (const Lexem& left, const Lexem& right);
bool operator != (const Lexem& left, const Lexem& right);
ostream& operator << (ostream& os, const Lexem& lex);



class Token {
    regex pattern;
    Tag tag;
    string string_pattern;

public:
    Token(const string& _template, const Tag& _tag);
    Token(const char* _template, const Tag& _tag);
    ~Token();

    Tag getTag() const { return tag; }
    string getPattern() const { return string_pattern; }

    Lexem match(const string& str, const unsigned int position=0) const;
};

ostream& operator << (ostream& os, const Token& token);
bool operator == (const Token& left, const Token& right);
bool operator != (const Token& left, const Token& right);



vector<Lexem> tokenize(const string& str, const vector<Token>& tokens);

#endif // LEXER_H
