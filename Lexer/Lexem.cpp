#include "Lexer.h"


ostream& operator << (ostream& os, const Tag& tag) {
    switch (tag) {
        case Tag::SEPARATOR:
            return os << "SEPARATOR";
        case Tag::REAL:
            return os << "REAL";
        case Tag::KEYWORD:
            return os << "KEYWORD";
        case Tag::IDENTIFIER:
            return os << "IDENTIFIER";
        case Tag::OPERATOR:
            return os << "OPERATOR";
        case Tag::BRACKETS:
            return os << "BRACKETS";
        case Tag::EOL:
            return os << "END_OF_LINE";
        case Tag::COMMA:
            return os << "COMMA";
        default:
            return os << "UNDEFINED";
    }
}


Lexem::Lexem() :
    content(string())
    , position(0)
    , length(0)
    , tag(Tag::UNDEFINED)
{}


Lexem::Lexem(
        const string &_content,
        const unsigned int _pos,
        const unsigned int _length,
        const Tag &_tag
    ) :
        content(_content)
        , position(_pos)
        , length(_length)
        , tag(_tag)
{}


void Lexem::set(
    const string& _content,
    const unsigned int _pos,
    const unsigned int _length,
    const Tag& _tag
) {
    content = _content;
    position = _pos;
    length = _length;
    tag = _tag;
}


bool Lexem::isEmpty() const {
    return (
        content == string() && position == 0 && length == 0 &&
        tag == Tag::UNDEFINED
    );
}


bool operator == (const Lexem& left, const Lexem& right) {
    return (
        left.getContent() == right.getContent() &&
        left.getPosition() == right.getPosition() &&
        left.getLength() == right.getLength() &&
        left.getTag() == right.getTag()
    );
}


bool operator != (const Lexem& left, const Lexem& right) {
    return !(left == right);
}


ostream& operator << (ostream& os, const Lexem& lex) {
    return os << "[Lexem: Content - '" << lex.getContent() << "'"
              << ", Position - " << lex.getPosition()
              << ", Length - " << lex.getLength()
              << ", Tag - " << lex.getTag() << "]";
}
